const expect = require('chai').expect;
const data = require('./index');

describe("the number of matches played per year of all the years in IPL.", function(){
    it("should exist", function(){
        expect(data.getMatchesPerYear).to.exist;
    })
    
    it("should not be empty", function(){
        expect(data.getMatchesPerYear()).to.be.not.undefined;
    })
    
    it("should not return null", function(){
        expect(data.getMatchesPerYear()).to.be.not.null;
    })

    it("should return an array object", function(){
        expect(data.getMatchesPerYear()).to.be.an('object')
    })

    it("should be the expected result", function(){
        const fileName = "demo.csv";

        let expectedResult = {
            2008: 5,
            2016: 7
        }
            expect(data.getMatchesPerYear(fileName)).equal(expectedResult);

        
    })

})